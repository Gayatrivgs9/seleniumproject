package testCases;

import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;

import pages.LoginPage;
import wdMethods.ProjectMethods;

public class TC001_CreateLead extends ProjectMethods {

	@BeforeTest
	public void setData() {
		testCaseName = "TC001_CreateLead";
		testDescription ="create a new lead";
		category = "Smoke";
		authors= "Gayatri";
		testNodes = "Leads";
		dataSheetName="TC001";
	}
	@Test(dataProvider="fetchData")
	public  void loginAndLogout(String uname, String password, String cname, String fname, String lname, String email){
		new LoginPage()
		.typeUsername(uname)
		.typePassword(password)
		.clickLogin()
		.clickCrmsfa()
		.clickLeads()
		.clickCreateLead()
		.enterCompanyName(cname)
		.enterFirstName(fname)
		.enterLastName(lname)
		.enteremail(email)
		.clickCreateleadButton()
		.verifyFirstname(fname);
	}


}









