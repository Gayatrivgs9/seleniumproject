package testCases;

import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;

import pages.DeleteLeadPage;
import pages.LoginPage;
import wdMethods.ProjectMethods;

public class TC002_DeleteLead extends ProjectMethods {

	@BeforeTest
	public void setData() {
		testCaseName = "TC002_DeleteLead";
		testDescription ="Delete the lead";
		category = "Sanity";
		authors= "Gayatri";
		testNodes = "Leads";
		dataSheetName="TC003";
	}
	@Test(dataProvider="fetchData")
	public  void loginAndLogout(String uname, String password, String email, String errorMsg) throws InterruptedException{
		new LoginPage()
		.typeUsername(uname)
		.typePassword(password)
		.clickLogin()
		.clickCrmsfa()
		.clickLeads()
		.clickFindLeads()
		.clickEmail()
		.typeEmailAddress(email)
		.clickFindButton()
		.getFirstResultLeadIdText()
		.clickFirstResultLeadId();
		new DeleteLeadPage()  
		.clickDeleteLink()
		.clickFindLeads()
		.typeLeadId()
		.clickFindButton()
		.verifyErrormsg(errorMsg);
		
	}


}









