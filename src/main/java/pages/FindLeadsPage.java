package pages;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

import wdMethods.ProjectMethods;

public class FindLeadsPage extends ProjectMethods {

	public FindLeadsPage() {
	PageFactory.initElements(driver, this);
	}
	
	@FindBy(xpath="//span[text()='Email']")
	WebElement eleEmail;
	public FindLeadsPage clickEmail() {
        click(eleEmail);
		return this;
	}
	
	@FindBy(xpath="//input[@name='emailAddress']")
	WebElement eleEmailAddress;
	public FindLeadsPage typeEmailAddress(String data) {
	type(eleEmailAddress, data);
	return this;
	}
	
	@FindBy(xpath="//input[@name='id']")
	WebElement eleLeadId;
	public FindLeadsPage typeLeadId() {
	type(eleLeadId, text); 
	return this;
	}
	
	@FindBy(xpath="//button[text()='Find Leads']")
	WebElement eleFindButton;
	public FindLeadsPage clickFindButton() throws InterruptedException {
	click(eleFindButton);
	Thread.sleep(1000); 
	return this;
	}
	
	@FindBy(xpath="//div[@class='x-grid3-cell-inner x-grid3-col-partyId']/a")
	WebElement eleFirstResultLeadId;
	public FindLeadsPage clickFirstResultLeadId() {
		click(eleFirstResultLeadId); 
		return this;
	}
	
	@FindBy(xpath="//div[@class='x-grid3-cell-inner x-grid3-col-partyId']/a")
	WebElement eleGetResultLeadIdText;
	public FindLeadsPage getFirstResultLeadIdText() {
		text = eleGetResultLeadIdText.getText();
		return this;
	}
	
	@FindBy(className="x-paging-info")
	WebElement eleErrormsg;
	public FindLeadsPage verifyErrormsg(String data) {
		verifyExactText(eleErrormsg, data);  
		return this; 
	}
}
