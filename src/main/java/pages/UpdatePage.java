package pages;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

import wdMethods.ProjectMethods;

public class UpdatePage extends ProjectMethods {

	public UpdatePage() {
	PageFactory.initElements(driver, this);
	}
	@FindBy(id="updateLeadForm_companyName")
	WebElement eleUpdateName;
	public UpdatePage typeUpdateName(String data) {
	type(eleUpdateName, data);
	return this;
	}
	
	@FindBy(className="smallSubmit")
	WebElement eleLUpdate;
	public UpdatePage clickUpdateButton() {
	click(eleLUpdate);
	return this;
	}
}







