package pages;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

import wdMethods.ProjectMethods;

public class ViewLeadPage extends ProjectMethods {

	public ViewLeadPage() {
	PageFactory.initElements(driver, this);
	}
	@FindBy(id="viewLead_firstName_sp")
	WebElement eleVerify;
	public ViewLeadPage verifyFirstname(String data) {
	verifyExactText(eleVerify, data);
	return this;
	}
}







