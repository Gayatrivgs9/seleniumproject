package pages;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

import wdMethods.ProjectMethods;

public class EditLeadPage extends ProjectMethods {

	public EditLeadPage() {
		PageFactory.initElements(driver, this);
	}
	
	@FindBy(linkText="Edit")
	WebElement eleEditLead;
	
	public UpdatePage clickEditLink(){
		click(eleEditLead);
		return new UpdatePage(); 
   }


}




