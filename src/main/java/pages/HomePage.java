package pages;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

import wdMethods.ProjectMethods;

public class HomePage extends ProjectMethods {

	public HomePage() {
	PageFactory.initElements(driver, this);
	}
	@FindBy(className="decorativeSubmit")
	WebElement eleLogOut;
	public MyHomePage clickLogOut() {
	click(eleLogOut);
	return new MyHomePage();
	}
	
	@FindBy(linkText="CRM/SFA")
	WebElement eleLCrmsfa;
	public MyHomePage clickCrmsfa() {
	click(eleLCrmsfa);
	return new MyHomePage();
	}
}







