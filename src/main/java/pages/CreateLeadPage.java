package pages;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

import wdMethods.ProjectMethods;

public class CreateLeadPage extends ProjectMethods {

	public CreateLeadPage() {
		PageFactory.initElements(driver, this);
	}
	@FindBy(id="createLeadForm_companyName")
	WebElement eleCname;
	@FindBy(id="createLeadForm_firstName")
	WebElement eleFname;
	@FindBy(id="createLeadForm_lastName")
	WebElement eleLname;
	@FindBy(id="createLeadForm_primaryEmail")
	WebElement eleemail;
	@FindBy(className="smallSubmit")
	WebElement eleCreateleadButton;
	
	public CreateLeadPage enterCompanyName(String data) {
		type(eleCname, data);
		return this;
	}
	public CreateLeadPage enterFirstName(String data) {
		type(eleFname, data);
		return this;
	}
	public CreateLeadPage enterLastName(String data) {
		type(eleLname, data);
		return this;
	}
	public CreateLeadPage enteremail(String data) {
		type(eleemail, data);
		return this;
	}
	public ViewLeadPage clickCreateleadButton(){
		click(eleCreateleadButton);
		return new ViewLeadPage();
   }


}




