package pages;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

import wdMethods.ProjectMethods;

public class DeleteLeadPage extends ProjectMethods {

	public DeleteLeadPage() {
		PageFactory.initElements(driver, this);
	}
	@FindBy(linkText="Delete")
	WebElement eleDelete;
	public MyLeadsPage clickDeleteLink() {
          click(eleDelete);
		return new MyLeadsPage();
	}
	


}




